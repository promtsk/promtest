<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

?>

<? $APPLICATION->IncludeComponent(
    "2quick:adv.list",
    '',
    [
        "HL_BLOCK_ID" => $arParams["HL_BLOCK_ID"],
        "HL_BLOCK_ID_ADV" => $arParams["HL_BLOCK_ID_ADV"],
        "TYPES_IBLOCK_ID" => $arParams["TYPES_IBLOCK_ID"],
        "AJAX_MODE" => $arParams["AJAX_MODE"],
    ],
    false
); ?>

