<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

?>

<div id="content">
	<form id="form-filter" action="<?=POST_FORM_ACTION_URI?>" method="GET">

		<div class="field">
			<label>Направление:</label>
			<select name="filter[direction]">
				<option value="">Не выбрано</option>
                <?php foreach ($arResult['DIRECTIONS'] as $arDirection){ ?>
					<option value="<?php echo $arDirection['ID'];?>"
						<?php echo $arDirection['ID'] == $arResult['FILTER']['UF_DIRECTION']?'selected':''?>>
						<?php echo $arDirection['NAME']; ?>
					</option>
                <?php } ?>
			</select>
		</div>
		<div class="field">
			<label>Реклама:</label>
			<select name="filter[type]">
				<option value="">Не выбрано</option>
                <?php foreach ($arResult['TYPES'] as $arType){ ?>
					<option value="<?php echo $arType['ID'];?>"
						<?php echo $arType['ID'] == $arResult['FILTER']['UF_ADV']?'selected':''?>>
						<?php echo $arType['NAME']; ?>
					</option>
                <?php } ?>
			</select>
		</div>
		<div class="field">
			<label>Город:</label>
			<select name="filter[city]">
				<option value="">Не выбрано</option>
                <?php foreach ($arResult['CITYS'] as $arCity){ ?>
					<option value="<?php echo $arCity['ID'];?>"
                        <?php echo $arCity['ID'] == $arResult['FILTER']['UF_CITY']?'selected':''?>>
                        <?php echo $arCity['NAME']; ?>
					</option>
                <?php } ?>
			</select>
		</div>
		<div class="field tqDates">
			<label>Период:</label>
			<input type="date" name="filter[date_start]" value="<?=
            (!empty($arResult['FILTER'][">=UF_DATE"])?
                date('Y-m-d',strtotime($arResult['FILTER'][">=UF_DATE"])):'')
            ?>"> -
			<input type="date" name="filter[date_end]" value="<?=
			(!empty($arResult['FILTER']["<=UF_DATE"])?
			date('Y-m-d',strtotime($arResult['FILTER']["<=UF_DATE"])):'')
			?>">
		</div>
		<div class="field">
			<button type="submit" name="submit" value="Y" class="search">Поиск</button>
			<button type="reset" class="refresh">Очистить</button>
		</div>
	</form>

    <?php if(!empty($arResult['ITEMS'])){ ?>
		<div id="list">
			<table>
				<tr id="header-list">
					<td>Направление</td>
					<td>Площадка</td>
					<td>Затраты</td>
					<td>Город</td>
					<td>Дата</td>
					<td class="tqCenter">Файл выгрузки</td>
					<td>Управление</td>
				</tr>
                <?php foreach ($arResult['ITEMS'] as $ip => $item){ ?>
					<tr data-id="<?=$item['ID']?>">
						<td>
							<select name="UF_DIRECTION">
                                <?php foreach ($arResult['DIRECTIONS'] as $arDirection){ ?>
									<option value="<?= $arDirection['ID'];?>"
										<?= $arDirection['ID'] == $item['UF_DIRECTION']?'selected':''?>>
										<?php echo $arDirection['NAME']; ?>
									</option>
                                <?php } ?>
							</select>
						</td>
						<td>
							<select name="UF_ADV">
                                <?php foreach ($arResult['TYPES'] as $arType){ ?>
									<option value="<?= $arType['ID'];?>"
										<?= $arType['ID'] == $item['UF_ADV']?'selected':''?>>
										<?php echo $arType['NAME']; ?>
									</option>
                                <?php } ?>
							</select>
						</td>
						<td><input type="text" value="<?= $item['UF_VALUE'] ?>" name="UF_VALUE"></td>
						<td>
							<select name="UF_CITY">
                                <?php foreach ($arResult['CITYS'] as $arCity){ ?>
									<option value="<?= $arCity['ID'];?>"
                                        <?= $arCity['ID'] == $item['UF_CITY']?'selected':''?>>
                                        <?php echo $arCity['NAME']; ?>
									</option>
                                <?php } ?>
							</select>
						</td>
						<td><input type="text" value="<?= $item['UF_DATE'] ?>" name="UF_DATE"></td>
						<td><a download href="<?= $item['DOCUMENT'] ?>">Скачать</a></td>
						<td><a class="tqChange">Изменить</a></td>

					</tr>
                <?php } ?>
			</table>
			<?=$arResult['PAGER']?>
		</div>
    <?php } else { ?>
		<h2 class="empty-header">Данные отсутвуют</h2>
    <?php } ?>
</div>
<?if($_REQUEST['AJAX_CALL'] == 'Y'){?>
<script>
        BX.closeWait();
</script>
<?}?>