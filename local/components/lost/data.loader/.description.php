<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => "Загрузчик CSV данных",
	"DESCRIPTION" => "Компонент, который загружает файлы (google, yandex).csv в БД",
	"CACHE_PATH" => "Y",
	"SORT" => 30,
	"PATH" => array(
		"ID" => "content",
	),
);

?>