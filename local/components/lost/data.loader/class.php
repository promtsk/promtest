<?

use \Bitrix\Main;
use \Bitrix\Main\Loader;
use \Bitrix\Main\Error;
use \Bitrix\Main\Type\DateTime;
use \Bitrix\Main\Localization\Loc;
use \Bitrix\Iblock;
use \Bitrix\Iblock\Component\ElementList;
use \Bitrix\Main\Application;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

/**
 * @global CUser $USER
 * @global CMain $APPLICATION
 * @global CIntranetToolbar $INTRANET_TOOLBAR
 */
@set_time_limit(6000);
@ini_set('memory_limit', '2048M');
Loc::loadMessages(__FILE__);



if (!\Bitrix\Main\Loader::includeModule('iblock')) {
    ShowError(Loc::getMessage('IBLOCK_MODULE_NOT_INSTALLED'));
    return;
}

Loader::includeModule("highloadblock");

use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

class LoadData extends ElementList
{
    /**
     * Overwritting component data
     *
     * @param array $params
     * @return array $params
     **/

    public function onPrepareComponentParams($params)
    {

        $params = parent::onPrepareComponentParams($params);

        $params['IBLOCK_TYPE'] = isset($params['IBLOCK_TYPE']) ? trim($params['IBLOCK_TYPE']) : '';

        return $params;
    }

    /**
     * Checking city
     *
     * @return null
     **/

    private function checkCities()
    {

        $city = $this->request->getPost('cities');

        if ($city) {
            $this->arResult['CHECK_CITIES_IS'] = $city;
        }
    }

    /**
     * Checking using ajax
     *
     * @param
     * @return bool
     **/

    private function isAjax()
    {
        return ($this->request->getPost('AJAX_CALL') == 'Y') ? true : false;
    }

    /**
     * Getting advertising from post require
     *
     * @return int $advID
     **/

    private function getAdvertising()
    {
        return (int)$this->request->getPost('adv');
    }

    /**
     * Getting direction from post require
     *
     * @return int $directionID
     **/

    private function getDirection()
    {
        return (int)$this->request->getPost('direction');
    }

    /**
     * Getting city ID
     *
     * @param string $cityName
     * @return int ID || boolean
     **/

    private function getCityIDByName($cityName)
    {
        $arr = $this->arCityLower;
        $cityName = trim($cityName);
        $key = array_search(mb_strtolower($cityName), $arr);
        if ($key === false) return false;
        $ID = intval($key);
        return $ID > 0 ? $ID : false;
    }

    /**
     * Parsing city array
     *
     * @param array $arr
     * @return int ID || boolean
     **/

    private function parcingCityArray($arr)
    {
        $ID = $this->getCityIDByName($arr[2]);
        return $ID && $ID > 0 ? $ID : false;

        foreach ($arr as $name) {
            $ID = $this->getCityIDByName($name);
            if (!$ID || $ID < 0) {
                continue;
            }
            return $ID;
        }
        return false;
    }

    /**
     * Checking element in DB
     *
     * @param int $ID
     * @return boolean
     **/

    private function checkingItemByID($ID)
    {
        $res = CIBlockElement::GetList(Array("SORT" => "ASC"),
            Array('IBLOCK_ID' => $this->arParams['IBLOCK_ID'], 'ID' => $ID), false, array('nTopCount' => 1),
            Array('ID'));
        return $res->GetNext() ? true : false;
    }

    /**
     * Updating element in DB
     *
     * @param int $ID
     * @param array $item
     * @return null
     **/

    private function updateElementByID($ID, $item)
    {
        $el = new CIBlockElement;
        $arLoadProductArray = Array(
            'IBLOCK_SECTION' => false,          // элемент лежит в корне раздела
            'NAME' => $item['NAME'],
            'CODE' => $item['CODE'],
            'ACTIVE' => !$item['ACTIVE'] ? 'N' : 'Y',
        );

        if ($el->Update($ID, $arLoadProductArray)) {
            CIBLockELement::SetPropertyValuesEx($ID, $this->arParams['IBLOCK_ID'], array(
                'COL_CITY' => $item['PROPERTY_COL_CITY'],
                'COL_DATE' => $item['PROPERTY_COL_DATE'],
                'COL_PRICE' => $item['PROPERTY_COL_PRICE']
            ));
        }
    }

    /**
     * Adding element in DB
     *
     * @param array $item
     * @return null
     **/

    private function addElementByID($item)
    {
        $el = new CIBlockElement;

        $arLoadProductArray = Array(
            'IBLOCK_SECTION' => false,          // элемент лежит в корне раздела
            'IBLOCK_ID' => $this->arParams['IBLOCK_ID'],
            'NAME' => $item['NAME'],
            'CODE' => $item['CODE'],
            'ACTIVE' => !$item['ACTIVE'] ? 'N' : 'Y',
            'PROPERTY_VALUES' => array(
                'COL_CITY' => $item['PROPERTY_COL_CITY'],
                'COL_DATE' => $item['PROPERTY_COL_DATE'],
                'COL_PRICE' => $item['PROPERTY_COL_PRICE']
            )
        );

        $el->Add($arLoadProductArray);

        if ($elID = $el->getId()) {
            $_SESSION['ADDED_ELEMENTS'][$elID] = $elID;
        }
    }

    /**
     * Validating form data
     *
     * @return boolean
     **/

    private function validateForm()
    {
        if ($this->request->getPost('download') == 'Y') {
            //проверка на наличие городов в ajax запросе
            $this->checkCities();

            //проверка на наличие типов документам в ajax запросе
            $this->checkType();

            //проверка на наличие направлений в ajax запросе
            $this->checkDirection();

            if (empty($this->getDirection())) {
                $this->arResult['ERRORS'][] = GetMessage('EMPTY_DIRECTION');
            }

            if (empty($this->getAdvertising())) {
                $this->arResult['ERRORS'][] = GetMessage('EMPTY_ADV');
            }

            $file = $this->request->getFile('file');
            if (empty($file['tmp_name']) && empty($_SESSION['FILE_DATA'])) {
                $this->arResult['ERRORS'][] = GetMessage('EMPTY_FILE');
            } else {
                //pre($_SESSION['FILE_DATA'],'session file data when empty arrays');
                if (!$this->getRightData()) {
//                    pre('not right data !!!');
                    $_SESSION['FILE_DATA'] = array();
                    $this->getRightData();
                }
            }
        } elseif ($this->request->getPost('edit_form') == 'Y') {
            $items = $this->request->getPost('ITEMS');
            $getFullList = false;
            foreach ($items as $ID => $item) {
                if (empty($item['NAME'])) {
                    $this->arResult['ERRORS'][] = GetMessage('EMPTY_EDIT_NAME');
                } elseif (empty($item['CODE'])) {
                    $this->arResult['ERRORS'][] = GetMessage('EMPTY_EDIT_CODE');
                } elseif (empty($this->arResult['ERRORS']) && $this->checkingItemByID($ID)) {
                    $this->updateElementByID($ID, $item);
                } elseif (empty($this->arResult['ERRORS'])) {
                    $this->addElementByID($item);
                    $getFullList = true;
                }
            }
            if ($getFullList) {
                $this->setTypes();
                $this->arResult['ITEMS'] = $this->arType;
            }
        }

//        pre($_SESSION['FILE_DATA'],'validate end session FD');
        if (isset($this->arResult['ERRORS']) && !empty($this->arResult['ERRORS'])) {
            return false;
        }
        return true;
    }

    /**
     * check .csv file titles
     *
     * @param array $arrFirstRow
     * @return array
     **/

    private function checkCsvColumns($arrFirstRow)
    {
        $type = $this->getAdvertising();

        $cityName = $this->arType[$type]['PROPERTY_COL_CITY_VALUE'];
        $priceName = $this->arType[$type]['PROPERTY_COL_PRICE_VALUE'];
        $dateName = $this->arType[$type]['PROPERTY_COL_DATE_VALUE'];

//        global $USER;
//        if ($USER->GetID() == 5) {
//            pre_dump($arrFirstRow, '$arrFirstRow');
//            pre($arrFirstRow, '$arrFirstRow');
//            pre($this->arType[$type],'col names by type');
//        }
//        if (!function_exists('asdsad23')) {
//            function asdsad23($str)
//            {
//                $charset = mb_detect_encoding($str);
//
//                $str = iconv($charset, 'UTF-8', $str);
//
//                return str_replace(array('\'', '"'), '', $str);
//            }
//        }
//        $arrFirstRow = array_map('asdsad23', $arrFirstRow);

        $cityKey = array_search($cityName, $arrFirstRow);
        $priceKey = array_search($priceName, $arrFirstRow);
        $datekey = array_search($dateName, $arrFirstRow);

//        if ($USER->GetID() == 5) {
//            pre_dump($cityKey, '$cityKey');
//            pre_dump($priceKey, '$priceKey');
//            pre_dump($datekey, '$datekey');
//        }

        if ($cityKey === false) {
            $this->arResult['ERRORS'][] = GetMessage('CANNOT_FIND_CITY');
        }
        if ($priceKey === false) {
            $this->arResult['ERRORS'][] = GetMessage('CANNOT_FIND_PRICE');
        }
        if ($datekey === false) {
            $this->arResult['ERRORS'][] = GetMessage('CANNOT_FIND_DATE');
        }

        return !empty($this->arResult['ERRORS'])
            ? false
            : array(
                'city' => $cityKey,
                'price' => $priceKey,
                'date' => $datekey
            );
    }


    /**
     * Breaking a first step of data
     *
     * @return array $data || boolean
     **/

    private function getDocumentRows()
    {

        if (!empty($this->request->getPost('TQ_CURRENT_FILE'))) {
            $file = CFile::MakeFileArray($this->request->getPost('TQ_CURRENT_FILE'));
            $this->arResult['CURRENT_FILE'] = CFile::GetFileArray($this->request->getPost('TQ_CURRENT_FILE'));

        } else {
            $file = $this->request->getFile('file');
            $this->arResult['CURRENT_FILE'] = $file;
        }

        if (!$file) {
            return false;
        }
        if (($handle = fopen($file['tmp_name'], "r")) === false) {
            return false;
        }
        $data = array();
        $rowNum = 0;
        $stringMaxLength = 500;
        $delimeter = getCsvDelimeter($handle, $stringMaxLength);
        while ($row = fgetcsv($handle, $stringMaxLength, $delimeter)) {
            //обработка первой строки - заголовков
            if (!$rowNum++) {
                if (count($row) < 3) {
                    return false;
                }
            }
            $data[] = $row;
        }

        //if(!empty($file['ID'])) CFile::Delete($file['ID']);

        return is_array($data) && !empty($data) ? $data : false;
    }

    /**
     * Getting right data
     *
     * @return array $data || boolean
     *
     **/
    private function getRightData()
    {
        $data = '';

        if ($this->request->getPost('download') == 'Y') {
            if (!$this->getDocumentRows() && !$_SESSION['FILE_DATA_CONTINUE']&& !$_SESSION['WRONG_DATA']) {
                $this->arResult['ERRORS'][] = GetMessage('EMPTY_IS_NOT_CORRECT');
            } elseif (empty($_SESSION['FILE_DATA'])) {
                $data = $this->getDocumentRows();
                $_SESSION['FILE_DATA'] = is_array($data) ? $data : array();

////                $type = $this->getCodeFromItems();
//                $type = $this->getAdvertising();
//
//                if (isset($this->arType[$type])) $this->arResult['FILE_DATA'] = $this->getTypeResult($data);
//                else {
//                    $data = null;
//                    $this->arResult['ERRORS'][] = GetMessage('EMPTY_IS_NOT_CORRECT');
//                }
//            } elseif (!empty($_SESSION['FILE_DATA']) && is_array($_SESSION['FILE_DATA'])) {
//                $this->arResult['FILE_DATA'] = $_SESSION['FILE_DATA'];
            }
        }
        return !is_null($data);
    }

    //    /**
    //     * Parsing yandex file
    //     *
    //     * @param array $arr
    //     * @return array $data
    //     **/
    //
    //    private function getTypeResult($arr)
    //    {
    //        $_SESSION['FILE_DATA'] = array();
    //        if (is_array($arr)) $_SESSION['FILE_DATA'] = $arr;
    //        return $arr;
    //    }

    /**
     * Checking type
     *
     * @return null
     **/

    private function checkType()
    {
        $type = $this->getAdvertising();
        if ($type) {
            $this->arResult['CHECK_TYPE_IS'] = $type;
        }
    }

    /**
     * Checking data from HlBlock in DB
     *
     * @param int $cityID
     * @param string $date
     * @return boolean
     **/

    private function checkDataIntoHightLoadBlock($cityID, $date)
    {
        $block = HL\HighloadBlockTable::getById($this->arParams['HIGHTBLOCK_ID']);

        $exist = false;

        if ($hlblock = $block->fetch()) {
            $entity = HL\HighloadBlockTable::compileEntity($hlblock);
            $entity_data_class = $entity->getDataClass();
            $arFilter = array(
                "UF_CITY" => $cityID,
                "UF_ADV" => $this->getAdvertising(),
                "UF_DATE" => date("d.m.Y", strtotime($date)),
                'UF_DIRECTION' => $this->getDirection()
            );

//            pre($arFilter,'$arFilter');
            $rsData = $entity_data_class::getList(array(
                "filter" => $arFilter
            ));
            if ($arData = $rsData->Fetch()) {
                $_SESSION['ADDED_ELEMENTS'][$arData['ID']] = $arData['ID'];
                $exist = true;
            }

            return $exist;
        } else {
            die(GetMessage('EMPTY_HLBLOCK'));
        }
    }

    /**
     * Adding data from HlBlock in DB
     *
     * @param int $cityID
     * @param string $date
     * @param int $price
     * @return boolean
     **/

    private function addItemInBD($cityID, $date, $price)
    {

        $block = HL\HighloadBlockTable::getById($this->arParams['HIGHTBLOCK_ID']);

        if ($hlblock = $block->fetch()) {
            $entity = HL\HighloadBlockTable::compileEntity($hlblock);
            $entity_data_class = $entity->getDataClass();

            $data = array(
                "UF_CITY" => $cityID,
                "UF_DIRECTION" => $this->getDirection(),
                "UF_VALUE" => doubleval(str_replace(',', '.', $price)),
                "UF_ADV" => $this->getAdvertising(),
                "UF_DATE" => date("d.m.Y", strtotime($date)),
                'UF_TIMESTAMP' => $GLOBALS['HLDateTime']->format('d.m.Y H:i:s')
            );

            $result = $entity_data_class::add($data);
            if ($elID = $result->getId()) {
                $_SESSION['ADDED_ELEMENTS'][$elID] = $elID;
            }
        } else {
            die(GetMessage('EMPTY_HLBLOCK'));
        }
    }

    /**
     * Checking direction
     *
     * @return null
     **/

    private function checkDirection()
    {
        $direction = $this->getDirection();
        if ($direction) {
            $this->arResult['CHECK_DIRECTION_IS'] = $direction;
        }
    }


    /**
     * @param $arFile
     * @param $arElementsIDs
     * @throws Main\SystemException
     */

    private function addFileToHLBLock($arFile, $arElementsIDs)
    {
        global $USER;
        $block = HL\HighloadBlockTable::getById($this->arParams['HIGHTBLOCK_ID_PRICES']);

        if ($hlblock = $block->fetch()) {
            $entity = HL\HighloadBlockTable::compileEntity($hlblock);
            $entity_data_class = $entity->getDataClass();
            if (!empty($arFile['ID'])) {
                $arSaveFile = CFile::MakeFileArray($arFile['ID']);
            } else {
                $arSaveFile = $arFile;
            }
            $data = array(
                'UF_FILE' => $arSaveFile,
                'UF_ELEMENTS' => $arElementsIDs,
                'UF_DATE' => date('d.m.Y H:i:s'),
                'UF_USER_ID' => $USER->GetID(),
                'UF_USER_NAME' => $USER->GetFormattedName(),
            );

            $result = $entity_data_class::add($data);
            unset($_SESSION['ADDED_ELEMENTS']);
            if (!empty($arFile['ID'])) {
                CFile::Delete($arFile['ID']);
            }
        }
    }

    /**
     * Getting result
     *
     * @return null
     **/

    protected function getResult()
    {
        $mt = microtime(true);
        //массив городов
        $this->arResult['CITIES'] = $this->arCity;

        //массив типов рекламы
        $this->arResult['ITEMS'] = $this->arType;

        //массив направлений
        $this->arResult['DIRECTIONS'] = $this->arDirection;


        if ($this->isAjax()) {

            //проверка валидности формы
            if ($this->validateForm()) {

                //получение данных из сессии
                $data = $_SESSION['FILE_DATA'];
                if (!empty($data) && is_array($data)) {
                    //получаю первую строку из документа
                    $firstRow = $data[0];
                    $arKeys = $this->checkCsvColumns($firstRow);
//                    pre($this->getAdvertising(), 'adv');
//                    pre($this->getDirection(), 'dir');
//                    pre($data, 'data');

                    if (is_array($arKeys)) {
                        $dataRowsAmount = count($data);
                        if ($dataRowsAmount == 1) {
                            $_SESSION['FILE_DATA'] = array();
                        } else {
                            unset($_SESSION['FILE_DATA_CONTINUE'], $_SESSION['WRONG_DATA']);
                            foreach ($data as $i => $dataRow) {
                                if (!$i) {
                                    continue;
                                }
                                if (microtime(true) - $mt > 55) {
                                    $_SESSION['FILE_DATA_CONTINUE'] = true;
                                    break;
                                }
                                //получаю город из документа
                                $city = $data[$i][$arKeys['city']];
                                //получаю дату из документа
                                $date = $data[$i][$arKeys['date']];
                                //получаю стоимость из документа
                                $price = $data[$i][$arKeys['price']];
                                if ($city && $date) {
                                    //разбиваю город на части
                                    $cityArray = explode("_", $city);
                                    //получаю ID из списка городов
                                    $cityID = $this->parcingCityArray($cityArray);
                                    //получаю данные города выбранного пользователем
                                    $correctCity = $this->request->getPost('correct_city');
                                    //ловлю предыдущий запрос города и обновляю данные при их несоответствии
                                    if (!$cityID && !empty($_SESSION['CITY_EXIST']) && $_SESSION['CITY_EXIST'] != $city) {
                                        $correctCity = '';
                                    }
                                    //город не найден в списке
                                    if (!$cityID && empty($correctCity)) {
                                        //Вывод целой записи из файла с неправильным городом
                                        $_SESSION['WRONG_DATA']['HEADER'][] = $firstRow;
                                        $_SESSION['WRONG_DATA']['BODY'][] = $data[$i];
                                        //вывожу маркер об ошибке в шаблон
                                        $this->arResult['WRONG_CITY'] = 'Y';
                                        //записываю некорректный город в сессию
                                        $_SESSION['CITY_EXIST'] = $city;
                                        $tqFile = $this->request->getFile('file');
                                        if(!empty($tqFile)){
                                            $this->arResult['CURRENT_FILE'] = CFile::SaveFile($tqFile, 'tq_files');
                                            $this->arResult['CURRENT_FILE'] = CFile::GetFileArray($this->arResult['CURRENT_FILE']);
                                        }



                                        break;
                                    } //пользователь пропускает этот этап
                                    elseif (!$cityID && $correctCity == 'skip') {
                                        //удаляю запись из сессии
                                        unset($_SESSION['FILE_DATA'][$i]);
                                        continue;
                                    } //пользователь делает выбор города
                                    elseif (!$cityID && $correctCity > 0) {
                                        $cityID = $correctCity;
                                        //проверяю на наличие записи в базе
                                        $exist = $this->checkDataIntoHightLoadBlock($cityID, $date);

                                        if (!$exist) {
                                            //добавляю запись в базу
                                            $this->addItemInBD($cityID, $date, $price);
                                            //удаляю запись из сессии
                                            unset($_SESSION['FILE_DATA'][$i]);
                                        } else {
                                            $this->arResult['ERRORS'][] = GetMessage('CHOSEN_THE_SAME');
                                            //удаляю запись из сессии
                                            unset($_SESSION['FILE_DATA'][$i]);
                                            continue;
                                        }
                                    } //запись найдена в списке город
                                    elseif ($cityID) {
                                        //проверяю на наличие записи в базе
                                        $exist = $this->checkDataIntoHightLoadBlock($cityID, $date);
                                       
                                        if (!$exist) {
                                            //добавляю запись в базу
                                            $this->addItemInBD($cityID, $date, $price);
                                            //удаляю запись из сессии
                                            unset($_SESSION['FILE_DATA'][$i]);
                                        } else {
                                            //удаляю запись из сессии
                                            unset($_SESSION['FILE_DATA'][$i]);
                                            continue;
                                        }
                                    } else {
                                        //удаляю запись из сессии
                                        unset($_SESSION['FILE_DATA'][$i]);
                                        continue;
                                    }
                                } else {
                                    //удаляю запись из сессии
                                    unset($_SESSION['FILE_DATA'][$i]);
                                    continue;
                                }
                            }

                            $fileDataCount = count($_SESSION['FILE_DATA'])-1;
                            $this->arResult['completed'] = 'Добавлено ' . (count($data) - $fileDataCount) . ', осталось: ' . $fileDataCount;
                            $data = $_SESSION['FILE_DATA'];
                            if(!empty($this->arResult['CURRENT_FILE']) && empty($this->arResult['CURRENT_FILE']['ID'])){
                                $this->arResult['CURRENT_FILE'] = CFile::SaveFile($this->arResult['CURRENT_FILE'],'tq_files');
                                $this->arResult['CURRENT_FILE'] = CFile::GetFileArray($this->arResult['CURRENT_FILE']);
                            }

                            $dataRowsAmount = count($data);
                            if ($dataRowsAmount == 1) {
                                $_SESSION['FILE_DATA'] = array();
                            }

                            if($fileDataCount == 0){
                                if (!empty($_SESSION['ADDED_ELEMENTS']) && !empty($this->arResult['CURRENT_FILE'])) {
                                    $this->addFileToHLBLock($this->arResult['CURRENT_FILE'], $_SESSION['ADDED_ELEMENTS']);
                                }
                            }
                        }
                    }



                } else {
                    $_SESSION['FILE_DATA'] = array();
                }
            }
        } else {

            unset($_SESSION['ADDED_ELEMENTS']);
            $_SESSION['FILE_DATA'] = array();
        }

        if (empty($_SESSION['FILE_DATA'])){
            unset($_SESSION['FILE_DATA_CONTINUE'], $_SESSION['WRONG_DATA']);

        }
    }

    /**
     * Executing component
     *
     * @return null
     **/

    public function executeComponent()
    {
//        pre($_FILES, 'files');
//        pre($_POST, 'post');
//        pre($_GET, 'get');
//        pre($_SESSION, 'session before component execution');
        $this->setCities();
        $this->setDirections();
        $this->setTypes();

        $this->getResult();
        //$this->arResult = $this->request;
        $this->includeComponentTemplate();
//        pre($_SESSION, 'session after component execution');

    }

    /*
     * fill $arCity with format [{id:name}]
     */

    protected $arCity = [];
    protected $arCityLower = [];

    public function setCities()
    {
        $this->arCity = array();
        $getOurPvzCities = City::getOurPvzCities();
        foreach ($getOurPvzCities as $ourCityInfo) {
            $cityName = trim($ourCityInfo['NAME']);
            $this->arCity[$ourCityInfo['ID']] = $cityName;
            $this->arCityLower[$ourCityInfo['ID']] = mb_strtolower($cityName);
        }
    }

    /*
     * fill $arDirections with format [{id:name}]
     */

    public function setDirections()
    {
        $this->arDirection = array();
        $except = array(
            false,//без категории
            128,//диагностика (после ремонта)
//    129,//комплектующие для микро
            115,//запчасти
        );
        $sectionList = getLinearSectionList(Product::IBLOCK_ID, $except);
        $sectionList = generateLinearSectionListName($sectionList);

        foreach ($sectionList as $sectionInfo) {
            if (isset($sectionInfo['PARENT_NAMES']) && !empty($sectionInfo['PARENT_NAMES'])) {
                $sectionInfo['NAME'] = implode(' >> ', $sectionInfo['PARENT_NAMES']) . ' >> ' . $sectionInfo['NAME'];
            }
            $this->arDirection[$sectionInfo['ID']] = $sectionInfo['NAME'];
        }
    }

    /*
     * fill $arTypes with format [{id:name}]
     */

    public function setTypes()
    {
        $this->arType = array();
        $res = CIBlockElement::GetList(
            Array("NAME" => "ASC"),
            Array('IBLOCK_ID' => $this->arParams['IBLOCK_ID']),
            false,
            false,
            Array(
                'ID',
                'NAME',
                'CODE',
                'ACTIVE',
                'PROPERTY_COL_CITY',
                'PROPERTY_COL_DATE',
                'PROPERTY_COL_PRICE'
            )
        );
        while ($element = $res->GetNext()) {
//            $this->arType[$element['CODE']] = $element['ID'];
            $this->arType[$element['ID']] = $element;
        }
    }
}