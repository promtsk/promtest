$(document)
    .ready(function (e) {
        var mainMenu = $('#main-menu'),
            amountElems = mainMenu.find('.get-amount');
        if (amountElems.length) {
            //load CNT
            $.ajax({
                type: "POST",
                url: "http://request.rus-crm.ru/ajax/new/getMenuNotifications.php",
                global: false,
                data: {
                    sessid: BX.bitrix_sessid(),
                    user: window.user_data.id,
                    user_code: window.user_data.code,
                    get_amount: 'main_menu',
                    ajax: 'Y'
                },
                timeout: 30000,
                dataType: 'json',
                success: function (result) {
                    if (!('success' in result)) return;

                    $.each(amountElems, function (ind, menuElem) {
                        var replaceAmount = '',
                            menuLi = $(menuElem.closest('li')),
                            menuCode = menuLi.data('amount');
                        if (menuCode in result.success) {
                            menuLi.addClass('amount');
                            replaceAmount = result.success[menuCode];
                        }
                        $(menuElem).replaceWith(replaceAmount);
                    });
                }
            });
        }
    });