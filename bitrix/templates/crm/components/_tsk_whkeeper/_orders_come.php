<?
/*
 * Игорю выведем уведомление, о приходе отправок на склад, который нужно забрать
 * отправки сдэка с отметкой "доставлено"
 */
//get tomsk warehouses:
$arWarehouses = array();
$db = CIBLockElement::GetList(
    array(),
    array(
        'IBLOCK_ID' => Warehouse::IBLOCK_ID,
        'ACTIVE' => 'Y',
        'PROPERTY_CITY' => City::Tsk,//tomsk
        '!PROPERTY_SALES' => false,
        '!PROPERTY_OUR_PVZ' => false
    ),
    false,
    false,
    array('ID')
);
while ($res = $db->Fetch())
    $arWarehouses[] = $res['ID'];
if (empty($arWarehouses) && !isset($_GET['vsyravno'])) return true;

//get orders
$db = CIBlockElement::GetList(
    array('ID' => 'ASC'),
    array(
        'IBLOCK_ID' => Purchase::IBLOCK_ID,
        'ACTIVE' => 'Y',
        'PROPERTY_ORDER_STATUS' => Purchase::STATUS_TRAVELLING,
        'PROPERTY_DELIVERY_STATUS' => 348,//dostvleno
        'PROPERTY_REC_WAREHOUSE' => $arWarehouses
    ),
    false,
    array('nTopCount' => 25),
    array(
        'ID',
        'PROPERTY_WAREHOUSE',
        'PROPERTY_WAREHOUSE.NAME',
        'PROPERTY_REC_WAREHOUSE',
        'PROPERTY_REC_WAREHOUSE.NAME'
    )
);

$arOrders = array();
while ($res = $db->Fetch()) {
    if ($res['PROPERTY_WAREHOUSE_VALUE'] == $res['PROPERTY_REC_WAREHOUSE_VALUE']) continue;
    $arOrders[] = $res;
}
if (empty($arOrders) && !isset($_GET['vsyravno'])) return true;

$popupContent = '<div id="_pechken"></div><div id="_chain"></div><div id="_glasses"></div><b>В Сдэке тебя ждет груз! Быстрее забери его, клиенты очень ждут</b><br/>';
foreach ($arOrders as $order)
    $popupContent .= '<div><a target="_blank" href="/purchase/?sid=' . $order['ID'] . '">' . $order['ID'] . '</a> ' . $order['PROPERTY_WAREHOUSE_NAME'] . ' - ' . $order['PROPERTY_REC_WAREHOUSE_NAME'] . '</div>';

?>
<audio id="_audio" controls>
    <source src="<?= SITE_TEMPLATE_PATH; ?>/components/_tsk_whkeeper/thuglife.mp3" type="audio/mpeg">
</audio>
<link rel="stylesheet" media="all" type="text/css" href="<?= SITE_TEMPLATE_PATH ?>/components/_tsk_whkeeper/style.css"/>
<script>
    $(function () {
        var _audio = document.getElementById("_audio"),
            _audioDelay = 750;
        if (_audio) {
            _audio.addEventListener("play", function () {
                if ($('#igor-popup').length && !$('#igor-popup.thuglife').length)
                    setTimeout(function () {
                        $('#igor-popup').addClass('thuglife');
                        _audioDelay = 250;
                    }, _audioDelay);
            });
            _audio.addEventListener("pause", function () {
                if ($('#igor-popup').length && $('#igor-popup.thuglife').length)
                    $('#igor-popup').removeClass('thuglife');
            });
        }
        var IhaaaaarPopup = new BX.PopupWindow("igor-popup", null, {
            content: <?='\'', $popupContent, '\'';?>,
            closeIcon: {right: "20px", top: "10px"},
            closeByEsc : true,
            titleBar: {
                content: BX.create("h1", {
                    html: 'Вам посылка!',
                    'props': {'className': 'access-title-bar'}
                })
            },
            zIndex: 0,
            offsetLeft: 0,
            offsetTop: 0,
            events: {
                onPopupFirstShow: function () {
                    if (_audio)
                        $('#igor-popup').append(_audio);
                },
                onPopupClose: function () {
                    IhaaaaarPopup.destroy();
                }
            },
            draggable: {restrict: false},
            overlay: {backgroundColor: 'black', opacity: '80'}, /* затемнение фона */
            buttons: [
                new BX.PopupWindowButton({
                    text: "Ознакомился",
                    className: "webform-button-link-cancel",
                    events: {
                        click: function () {
                            this.popupWindow.close();
                        }
                    }
                })
            ]
        });

        IhaaaaarPopup.show();
    });</script>