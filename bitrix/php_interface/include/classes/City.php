<?php

/*класс для работы с городами*/

class City
{
    const IBLOCK_ID = 16;
    const CCL = 'CdekCityList';//hlb

    //office city id:
    const Tsk = 336;
    const Nsk = 338;
    const Msk = 339;
    const Ekb = 439;
    const Krs = 476;//krasnodar
    const Ksk = 407;//krasnoyarsk


    /*
     * делаем полное имя 'город - склад' без посторения названия города 
     */
    static function getFullName($warehouseName, $cityName, $pvzCode = false)
    {
        $warehouseName = trim($warehouseName);
        $cityName = trim($cityName);
        return ((strpos($warehouseName, $cityName) === 0) ? '' : ($cityName . ': ')) . $warehouseName
            . ($pvzCode !== false ? (' [пвз:' . ($pvzCode ? $pvzCode : '-') . ']') : '');
    }

    /*
     * возвращаем ID городов, в которых есть офис (флаг OFFICE)
     */
    static function getOfficesID()
    {
        $arCities = self::getOfficesInfo(array('ID'));
        $arCities = array_column($arCities, 'ID');
        return $arCities;
    }

    static function getOfficesInfo($arSelect)
    {
        $arCities = array();

        if (!is_array($arSelect) || empty($arSelect)) return $arCities();

        $db = CIBlockElement::GetList(
            array('NAME' => 'ASC'),
            array(
                'IBLOCK_ID' => self::IBLOCK_ID,
                '!PROPERTY_OFFICE' => false,
                'ACTIVE' => 'Y'
            ),
            false,
            false,
            $arSelect
        );
        while ($res = $db->Fetch())
            $arCities[] = $res;

        return $arCities;
    }

    static function getCityOfficesID()
    {
        $result = self::getOfficesID();
        return $result;
    }

    /*
     * выбрать все города, в которых есть наши ПВЗ
     */
    static function getOurPvzCitiesID()
    {
        $ourPvzCities = array();

//        $db = CIBlockElement::GetList(
//            array(),
//            array(
//                'IBLOCK_ID' => Warehouse::IBLOCK_ID,
//                'SECTION_ID' => false,
//                '!PROPERTY_OUR_PVZ' => false
//            ),
//            array('PROPERTY_CITY')
//        );
//        while ($res = $db->Fetch())
//            $ourPvzCities[] = intval($res['PROPERTY_CITY_VALUE']);

        return $ourPvzCities;
    }

    static function getOurPvzCities()
    {
        $ourPvzCities = array();

//        $db = CIBlockElement::GetList(
//            array('PROPERTY_CITY.NAME' => 'ASC'),
//            array(
//                'IBLOCK_ID' => Warehouse::IBLOCK_ID,
//                'SECTION_ID' => false,
//                '!PROPERTY_OUR_PVZ' => false,
//                'ACTIVE' => 'Y'
//            ),
//            array('PROPERTY_CITY', 'PROPERTY_CITY.NAME')
//        );
//        while ($res = $db->Fetch()) {
//            $ID = intval($res['PROPERTY_CITY_VALUE']);
//            $ourPvzCities[$ID] = array(
//                'ID' => $ID,
//                'NAME' => trim($res['PROPERTY_CITY_NAME'])
//            );
//        }

        return $ourPvzCities;
    }

    /*
     * по списку кодов СДЭКа вытащить список городов из файла,
     * если список НЕ задан, вернем весь список
     * если список задан, но пуст либо некорректен, то обработаем его (возможно вернем array())
     */
    static function getCdekCityListByCode($codeList = false)
    {
        $list = array();

        //если задан массив кодов для поиска
        if ($codeList !== false) {
            if (!is_array($codeList))
                $codeList = array($codeList);

            if (!empty($codeList)) {
                $codeList = array_map('intval', $codeList);//преобразуем всё в числа

                //удалим все 0 т.к. это лишняя проверка
                $arKey = array_keys($codeList, 0);
                if (!empty($arKey)) {
                    foreach ($arKey as $key)
                        unset($codeList[$key]);
                    //после удаления всех 0, если искать нечего, то вернем пустой массив
                    if (empty($codeList))
                        return $list;
                }
                unset($arKey);
            } else
                $codeList = false;
        }

        //get currency list
        list($arCurrency, $defaultCurrencyID) = self::getCurrencyList();

        $ourPvzCities = self::getOurPvzCitiesID();
        //сначала с ИБ
        $db = CIBlockElement::GetList(
            array('NAME' => 'ASC'),
            array(
                'IBLOCK_ID' => self::IBLOCK_ID,
                'ACTIVE' => 'Y',
                'PROPERTY_CDEK' => $codeList
            ),
            false,
            is_array($codeList) ? array('nTopCount' => count($codeList)) : false,
            array(
                'ID',
                'NAME',
                'IBLOCK_ID',
                'PROPERTY_CDEK',
                'PROPERTY_CURRENCY'
            )
        );
        while ($res = $db->Fetch()) {
            $type = in_array($res['ID'], $ourPvzCities) ? 'ourpvz' : 'otherpvz';
            $currency = $arCurrency[$res['PROPERTY_CURRENCY_ENUM_ID']]
                ? $arCurrency[$res['PROPERTY_CURRENCY_ENUM_ID']]
                : $arCurrency[$defaultCurrencyID];
            $list[$type][] = array(
                'code' => intval($res['PROPERTY_CDEK_VALUE']),
                'name' => trim($res['NAME']),
                'currency' => intval($currency['ID']),
                'currencyCode' => strtolower(trim($currency['XML_ID'])),
                'currencyName' => trim($currency['VALUE']),
            );
        }

        //теперь из ХЛБ
        $CCL = new HLBlock1(self::CCL);
        $db = $CCL->GetList(
            array('UF_NAME' => 'ASC'),
            $codeList !== false ? array('UF_CODE' => $codeList) : array(),
            false,
            $codeList !== false ? array('nPageSize' => count($codeList)) : false,
            array('UF_CODE', 'UF_NAME', 'UF_NALSUMLIM', 'UF_CITY_CURRENCY')
        );
        while ($res = $db->Fetch()) {
            $currency = $arCurrency[$res['UF_CITY_CURRENCY']]
                ? $arCurrency[$res['UF_CITY_CURRENCY']]
                : $arCurrency[$defaultCurrencyID];
            $list['other'][] = array(
                'code' => intval($res['UF_CODE']),
                'name' => trim($res['UF_NAME']),
                'nal_sum_lim' => trim($res['UF_NALSUMLIM']),
                'currency' => intval($currency['ID']),
                'currencyCode' => strtolower(trim($currency['XML_ID'])),
                'currencyName' => trim($currency['VALUE']),
            );
        }

        return $list;
    }

    static function getCityByName($name, $limit = 100)
    {
        if (!$name = trim($name))
            return false;

        $arNavStartParams = ($limit = intval($limit)) > 0 ? array('nTopCount' => $limit) : false;

        $names = explode(' ', $name);
        $name = '%' . (count($names) > 1 ? implode('% && %', $names) : $name) . '%';

        $list = array(
            'ourpvz' => array(),
            'otherpvz' => array(),
            'other' => array()
        );
        //explode $cityList on our pvz & not our pvz
        $ourPvzCities = self::getOurPvzCitiesID();

        //get currency list
        list($arCurrency, $defaultCurrencyID) = self::getCurrencyList();

        //search in iblocks
        $db = CIBlockElement::GetList(
            array('NAME' => 'ASC'),
            array(
                'IBLOCK_ID' => self::IBLOCK_ID,
                'ACTIVE' => 'Y',
                'SECTION_ID' => false,
                '?NAME' => $name,
                '!CODE' => 'other'
            ),
            false,
            $arNavStartParams,
            array('ID', 'NAME', 'PROPERTY_CDEK', 'PROPERTY_CURRENCY', 'ACTIVE')
        );
        while ($res = $db->Fetch()) {
            $cityType = in_array($res['ID'], $ourPvzCities) ? 'ourpvz' : 'otherpvz';
            $currency = $arCurrency[$res['PROPERTY_CURRENCY_ENUM_ID']]
                ? $arCurrency[$res['PROPERTY_CURRENCY_ENUM_ID']]
                : $arCurrency[$defaultCurrencyID];
            $list[$cityType][] = array(
                'code' => $res['PROPERTY_CDEK_VALUE'] ? intval($res['PROPERTY_CDEK_VALUE']) : -1,
                'name' => trim($res['NAME']),
                'active' => $res['ACTIVE'] == 'Y' ? 1 : 0,
                'currency' => intval($currency['ID']),
                'currencyCode' => strtolower(trim($currency['XML_ID'])),
                'currencyName' => trim($currency['VALUE']),
            );

            $limit--;
        }

        $CCL = new HLBlock1(self::CCL);
        $db = $CCL->GetList(
            array('UF_NAME' => 'ASC'),
            array('?UF_NAME' => $name),
            false,
            $limit > 0
                ? array('nPageSize' => $limit)
                : false,
            array('UF_NAME', 'UF_CODE', 'UF_NALSUMLIM', 'UF_CITY_CURRENCY')
        );
        while ($res = $db->Fetch()) {
            $currency = $arCurrency[$res['UF_CITY_CURRENCY']]
                ? $arCurrency[$res['UF_CITY_CURRENCY']]
                : $arCurrency[$defaultCurrencyID];
            $list['other'][] = array(
                'name' => trim($res['UF_NAME']),
                'code' => intval($res['UF_CODE']),
                'nal_sum_lim' => trim($res['UF_NALSUMLIM']),
                'currency' => intval($currency['ID']),
                'currencyCode' => strtolower(trim($currency['XML_ID'])),
                'currencyName' => trim($currency['VALUE']),
            );
        }

        return $list;
    }

    /*
     * вытащим список валют городов
     */
    static function getCurrencyList()
    {
        $arCurrency = array();
        $defaultCurrencyID = false;
        $db = CIBlockPropertyEnum::GetList(
            array('SORT' => 'ASC'),
            array(
                'IBLOCK_ID' => self::IBLOCK_ID,
                'CODE' => 'CURRENCY'
            )
        );
        while ($res = $db->Fetch()) {
            if (strtolower(trim($res['XML_ID'])) == 'rub')
                $defaultCurrencyID = $res['ID'];

            $arCurrency[$res['ID']] = $res;
        }
        return array($arCurrency, $defaultCurrencyID);
    }

    /*
     * получим код валюты из базы (по необходимости вытащим список валют, елси он НЕ передан)
     */
    static function getCurrencyCodeFromlistByID($currencyID, $currencyList = false, $currencyDefault = false)
    {
        if ($currencyList === false || !is_array($currencyList))
            list($currencyList, $currencyDefault) = self::getCurrencyList();

        if (isset($currencyList[$currencyID]))
            return $currencyList[$currencyID]['XML_ID'];
        elseif ($currencyDefault !== false && isset($currencyList[$currencyDefault]))
            return $currencyList[$currencyDefault]['XML_ID'];
        else
            return '';
    }

    /*
     * получим приставку (страну) к названию города
     * $currencyCode - код
     */
    static function getCountryByCurrency($currencyCode)
    {
        if ($currencyCode == 'KZ' || $currencyCode == 'KZT') return 'KZ';
        if ($currencyCode == 'BLR' || $currencyCode == 'BYR') return 'BLR';
        return '';
    }

    /*
     * $currencyCode - код в сдэке
     */
    static function getPricePrefixByCurrency($currencyCode)
    {
        if ($currencyCode == 'KZT') return '_KZ';
        if ($currencyCode == 'BYR') return '_BLR';
        return '';
    }
}