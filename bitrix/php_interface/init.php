<?
define('DEV_PHONE_MD5', 'ca0170e4ad4c4b0b0a4b5b9db21929b8');

if (!function_exists('checkDevPhone')) {
    function checkDevPhone($phone)
    {
        return md5($phone) == DEV_PHONE_MD5;
    }
}

global $USER;
if (!$USER) $USER = new CUser;


CModule::AddAutoloadClasses('', array(
    'HLBlock1' => '/bitrix/php_interface/include/classes/HLBlock.php',
    'City' => '/bitrix/php_interface/include/classes/City.php',
    'Product' => '/bitrix/php_interface/include/classes/Product.php',
));


/*
 * при переносе на новый хостинг проверить время при исполнении на кроне !!
 * если что, поменять часовой поез
 */
//date_default_timezone_set('Asia/Almaty');
date_default_timezone_set('Asia/Krasnoyarsk');

use Bitrix\Main\Type\DateTime;

$GLOBALS['HLDateTime'] = new Bitrix\Main\Type\DateTime();


if (!function_exists('getCsvDelimeter')) {
    function getCsvDelimeter($handle, $symbols = 500)
    {
        if (!$handle) throw new Exception('no file handle');
        $defaultDelimeter = ';';
        $correctDelimeter = array($defaultDelimeter, ',', "\t");
        foreach ($correctDelimeter as $delimeter) {//get correct delimeter
            //pre($delimeter, 'try this csv delimeter');
            while ($row = fgetcsv($handle, $symbols, $delimeter)) {
                rewind($handle);//reset file cursor to the begining
                if (count($row) < 2) break;//stop walking through file
                return $delimeter;
            }
        }
        return $defaultDelimeter;//default
    }
}

if (!function_exists('getLoaderRotation')) {
    function getLoaderRotation()
    {
        return '<img class="loaderRotation" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA4AAAAOCAYAAAAfSC3RAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAB6SURBVDhPY8AD5KA0yeAhlCYIkoDYEMIEA2SN8kBcBGFiApCm21AaBCqgNEjTNSB2B/NwAJAmPwgTDiyAGK8mkoEUEN+FYl6QAB7QD8QmEOZAAlDorQViJjAPFTACsSyEiQmKgRhX6IE0dUCYhAHRKQcd2EBpagIGBgAO+Q0AFqj4oQAAAABJRU5ErkJggg==">';
    }
}


if (!function_exists('removeLastSlash')) {
    function removeLastSlash($str)
    {
        return !is_string($str) ? '' : (substr($str, -1) != '/' ? $str : substr($str, 0, -1));
    }
}

if (!function_exists('pre')) {
    function pre($ar, $name = '[noname]')
    {
        echo '<pre style="background: #fff; color: #000; display: block; text-align: left; border: 1px dotted pink;">', $name, ' :: ';
        print_r($ar);
        echo '</pre>';
    }
}

if (!function_exists('pre_dump')) {
    function pre_dump($ar, $name = '[noname]')
    {
        echo '<pre style="background: #fff; color: #000; display: block; text-align: left; border: 1px dashed green;">', $name, ' :: ';
        var_dump($ar);
        echo '</pre>';
    }
}



if (!function_exists('sectionPermission')) {
    function sectionPermission($arGroupsAllowed = array(), $arUsersAllowed = array())
    {
        global $USER, $APPLICATION;

        if (!$USER->IsAuthorized()) {
            ?>
            <div class="auth-block">
                <? $APPLICATION->IncludeComponent("bitrix:system.auth.form", "", Array()); ?>
            </div>
        <? } elseif (!checkUsersGroupPermission($arGroupsAllowed) && !checkUsersPermission($arUsersAllowed))
            echo sectionUnavailable();
        else return true;
        return false;
    }
}

if (!function_exists('checkUsersGroupPermission')) {
    function checkUsersGroupPermission($arGroupsAllowed)
    {
        if (!$arGroupsAllowed) return false;
        if (!is_array($arGroupsAllowed))
            $arGroupsAllowed = array($arGroupsAllowed);
        global $USER;
        $userGroups = $USER->GetUserGroupArray();
        return (bool)array_intersect($arGroupsAllowed, $userGroups);
    }
}

if (!function_exists('checkUsersPermission')) {
    function checkUsersPermission($arUsersAllowed)
    {
        if (!$arUsersAllowed) return false;
        if (!is_array($arUsersAllowed))
            $arUsersAllowed = array($arUsersAllowed);
        global $USER;
        return in_array($USER->GetID(), $arUsersAllowed);
    }
}

if (!function_exists('exitJson')) {
    function exitJson($data, $success = false)
    {
        exit(json_encode(returnArray($data, $success)));
    }
}
if (!function_exists('returnArray')) {
    function returnArray($data, $success = false)
    {
        $type = $success ? 'success' : 'error';
        return array($type => $data);
    }
}



if (!function_exists('sectionUnavailable')) {
    function sectionUnavailable()
    {
        return '<style>.folder_not_available{color:crimson;padding:10px;margin:30px -5px;border:3px double crimson;border-left:none;border-right:none;text-align:center}</style>'
            . '<div class="folder_not_available">'
            . 'Раздел недоступен. Для получения доступа танцуйте'
            . '<br/><img src="/bitrix/templates/crm/images/dancingfox.webp" width="200"/>'
            . '</div>';
    }
}




if (!function_exists('getLinearSectionList')) {
    function getLinearSectionList($IBLOCK_ID, $exceptSections = false)
    {
        $arFilter = array('IBLOCK_ID' => $IBLOCK_ID, 'GLOBAL_ACTIVE' => 'Y', 'CHECK_PERMISSIONS' => 'N');
        if ($exceptSections) $arFilter['!ID'] = $exceptSections;
        return getLinearSectionListByFilter($arFilter);
    }
}

/*
 * вытащим из БД линейный список разделов (по фильтру)
 */
if (!function_exists('getLinearSectionListByFilter')) {
    function getLinearSectionListByFilter($arFilter)
    {
        $sectionList = [];
        if (!intval($arFilter['IBLOCK_ID'])) return $sectionList;

        $addZeroSection = true;//флаг добавления корневой категории
        if (key_exists('!ID', $arFilter)) {
            if (is_numeric($arFilter['!ID']))
                $arFilter['!ID'] = [$arFilter['!ID']];
            if (is_array($arFilter['!ID']) && !empty($arFilter['!ID']))
                if (in_array(0, $arFilter['!ID']) || in_array(false, $arFilter['!ID']))
                    $addZeroSection = false;
        }
        if ($addZeroSection)
            $sectionList[] = array(
                'ID' => 0,
                'NAME' => 'Без категории',
                'PARENT' => 0,
                'ACTIVE' => 1
            );

        $db = CIBlockSection::GetList(array('LEFT_MARGIN' => 'ASC', 'NAME' => 'ASC'),
            $arFilter,
            false,
            array('ID', 'NAME', 'IBLOCK_SECTION_ID', 'GLOBAL_ACTIVE'));
        while ($res = $db->Fetch()) {
            $sectionList[$res['ID']] = array(
                'ID' => intval($res['ID']),
                'NAME' => $res['NAME'],
                'PARENT' => intval($res['IBLOCK_SECTION_ID']),
                'ACTIVE' => $res['GLOBAL_ACTIVE'],
            );
        }
        return $sectionList;
    }
}

/*
 * формирование имен у разделов в линейном списке с учетом вложенности
 * т.е. для каждого раздела найдем имена всех его предков
 */
if (!function_exists('generateLinearSectionListName')) {
    function generateLinearSectionListName($sectionList)
    {
        $parentNames = array();
        $prevParent = $prevParentName = false;
        foreach ($sectionList as $key => $sectionInfo) {
            $parent = intval($sectionInfo['PARENT'] ? $sectionInfo['PARENT'] : $sectionInfo['IBLOCK_SECTION_ID']);
            if ($prevParent !== false) {
                if (!$parent) {
                    $parentNames = array();
                } elseif ($prevParent == $parent) {
                    //do nothing
                } elseif (isset($parentNames[$parent])) {
                    $cut = 0;
                    foreach ($parentNames as $id => $name) {
                        if ($id == $parent) break;
                        $cut++;
                    }
                    $parentNames = array_slice($parentNames, 0, $cut);
                } elseif ($prevParent != $sectionInfo['ID']) {
                    $parentNames[$prevParent] = $prevParentName;
                }
            }

            if (!empty($parentNames))
                $sectionList[$key]['PARENT_NAMES'] = $parentNames;

            $prevParent = $parent;
            $prevParentName = $sectionInfo['NAME'];
        }

        return $sectionList;
    }
}

RegisterModuleDependences("pull", "OnGetDependentModule", "your_module", "CYourModulePullSchema", "OnGetDependentModule");

class CYourModulePullSchema
{
    public static function OnGetDependentModule()
    {
        return Array(
            'MODULE_ID' => "your_module",
            'USE' => Array("PUBLIC_SECTION", 'ADMIN_SECTION')
        );
    }
}


